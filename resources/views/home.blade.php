<div class="grid place-items-center h-screen ">
    <div class="bg-blue-100 p-12 flex justify-between w-1/5">
        <div class="text">
            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                <a href="/export">Export</a>
            </button>
        </div>
        <div>
            <button class="bg-red-400 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                <a href="/report">Report</a>
            </button>
        </div>
    </div>
</div>

