<div class="p-12 flex justify-between w-4/5">
    <table class="table-auto w-4/5">
        <thead>
        <tr>
            <th>Team Code</th>
            <th>Number</th>
            <th>Name</th>
            <th>Position</th>
            <th>Height</th>
            <th>Weight</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($players as $player) { ?>
        <tr>
            <td><?php echo $player->team_code ?></td>
            <td><?php echo $player->number ?></td>
            <td><?php echo $player->name ?></td>
            <td><?php echo $player->pos ?></td>
            <td><?php echo $player->height ?></td>
            <td><?php echo $player->weight ?></td>
        </tr>
        <?php } ?>

        </tbody>
    </table>
</div>
