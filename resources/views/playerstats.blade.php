<div class="p-12 flex justify-between">
    <table>
        <tr>
            <th>Name</th>
            <th>Age</th>
            <th>Games</th>
            <th>Games Started</th>
            <th>Minutes Played</th>
            <th>Field Goals</th>
            <th>Field Goals Attempted</th>
            <th>3pt</th>
            <th>3pt Attempted</th>
            <th>2pt</th>
            <th>2pt Attempted</th>
            <th>Free Throws</th>
            <th>Free Throws Attempted</th>
            <th>Offensive Rebounds</th>
            <th>Defensive Rebounds</th>
            <th>Assists</th>
            <th>Steals</th>
            <th>Blocks</th>
            <th>Turnovers</th>
            <th>Personal Fouls</th>
            <th>Total Points</th>
            <th>Field Goals Pct</th>
            <th>3pt Pct</th>
            <th>2pt Pct</th>
            <th>Free Throws Pct</th>
            <th>Total Rebounds</th>
        </tr>
        <?php foreach ($players as $player) { ?>
        <tr>
            <td><?php echo $player->name ?></td>
            <td><?php echo $player->age ?></td>
            <td><?php echo $player->games ?></td>
            <td><?php echo $player->games_started ?></td>
            <td><?php echo $player->minutes_played ?></td>
            <td><?php echo $player->field_goals ?></td>
            <td><?php echo $player->field_goals_attempted ?></td>
            <td><?php echo $player['three_pt'] ?></td>
            <td><?php echo $player['three_pt_attempted'] ?></td>
            <td><?php echo $player['two_pt'] ?></td>
            <td><?php echo $player['two_pt_attempted'] ?></td>
            <td><?php echo $player->free_throws ?></td>
            <td><?php echo $player->free_throws_attempted ?></td>
            <td><?php echo $player->offensive_rebounds ?></td>
            <td><?php echo $player->defensive_rebounds ?></td>
            <td><?php echo $player->assists ?></td>
            <td><?php echo $player->steals ?></td>
            <td><?php echo $player->blocks ?></td>
            <td><?php echo $player->turnovers ?></td>
            <td><?php echo $player->personal_fouls ?></td>
            <td><?php echo $player->total_points ?></td>
            <td><?php echo $player->fg_pct ?></td>
            <td><?php echo $player->three_pct ?></td>
            <td><?php echo $player->two_pct ?></td>
            <td><?php echo $player->ft_pct ?></td>
            <td><?php echo $player->total_rebounds ?></td>
        </tr>
        <?php } ?>
    </table>
</div>
