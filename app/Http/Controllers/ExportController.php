<?php

namespace App\Http\Controllers;

use App\Interfaces\ExportData\ExportData;
use App\Interfaces\ExportFormat\ExportFormat;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class ExportController extends BaseController
{
    /**
     * Single method class
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function __invoke(Request $request)
    {
        $exportData = new ExportData($request->type);
        if (!$data =  $exportData->getData($request)) {
            return $exportData->getError();
        }

        return (new ExportFormat($data, $request->input('format')))->getFormattedData();
    }
}
