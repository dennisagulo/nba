<?php


namespace App\Interfaces\ExportFormat;



class Html implements ExportFormatInterface
{
    private $data = [];

    public function getFormattedContent($data)
    {
        $this->data = $data;
        $type       = request('type') ?? 'players';
        $method     = $type.'Html';
        return $this->$method();
    }

    private function playersHtml()
    {
        return view('layout', ['content' => view('players')->with(['players' => $this->data])]);
    }

    private function playerstatsHtml()
    {
        return view('layout', ['content' => view('playerstats')->with(['players' => $this->data])]);
    }
}