<?php

namespace App\Interfaces\ExportFormat;


interface ExportFormatInterface
{
    public function getFormattedContent($data);
}