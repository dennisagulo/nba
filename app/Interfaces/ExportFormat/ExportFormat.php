<?php


namespace App\Interfaces\ExportFormat;


class ExportFormat
{
    private $data;

    private $format;

    const formats = [
        'html',
        'json',
        'xml',
        'serialize'
    ];

    private $type;

    public function __construct($data, $format){
        $this->data = $data;
        $format = $format ?? 'html';
        $this->format = ucfirst(strtolower($format));
    }

    /**
     * Get the data base on the required format!
     * @return mixed
     */
    public function getFormattedData()
    {
        $class  = 'App\Interfaces\ExportFormat\\'.$this->format;
        return (new $class)->getFormattedContent($this->data);
    }

}