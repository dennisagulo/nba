<?php


namespace App\Interfaces\ExportFormat;

use LSS\Array2XML;

class Xml implements ExportFormatInterface
{
    /**
     * Format the content
     *
     * @param $data
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getFormattedContent($data)
    {
        $data = is_array($data) ? $data : $data->toArray();
        $xml = Array2XML::createXML('data', [
            'entry' => $data
        ]);
        return response($xml->saveXML(), 200, [
            'Content-Type' => 'application/xml'
        ]);
    }
}