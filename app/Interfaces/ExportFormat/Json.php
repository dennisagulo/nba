<?php


namespace App\Interfaces\ExportFormat;


class Json implements ExportFormatInterface
{
    private $data = [];

    public function getFormattedContent($data)
    {
        return json_encode($data);
    }
}