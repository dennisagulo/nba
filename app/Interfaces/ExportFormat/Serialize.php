<?php


namespace App\Interfaces\ExportFormat;


class Serialize implements ExportFormatInterface
{
    private $data = [];

    public function getFormattedContent($data)
    {
        return serialize($data);
    }
}