<?php


namespace App\Interfaces\ExportData;




use App\Models\Roster;
use Illuminate\Http\Request;

class Players implements ExportDataInterface
{
    public function getData(Request $request)
    {
        return (new Roster())->getCollection($request);
    }
}