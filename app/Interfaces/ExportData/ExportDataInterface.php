<?php

namespace App\Interfaces\ExportData;

use Illuminate\Http\Request;

interface ExportDataInterface
{
    public function getData(Request $request);
}