<?php

namespace App\Interfaces\ExportData;


class ExportData
{
    const types = [
        'players',
        'playerstats',
        'teams',
        'teamstats'
    ];

    private $error;

    private $type;

    public function __construct($type){
        $this->type = ucfirst(strtolower($type));
    }

    public function getData($request)
    {
        if (!in_array(strtolower($this->type), self::types)) {
            $this->setError('Invalid data type '.$this->type);
            return false;
        }

        $class  = 'App\Interfaces\ExportData\\'.$this->type;
        return (new $class)->getData($request);
    }

    private function setError($error)
    {
        $this->error = $error;
    }

    public function getError()
    {
        return $this->error;
    }
}