<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class Roster extends Model
{
    protected $table = 'roster';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [];

    public function getCollection(Request $request)
    {
        $query =  QueryBuilder::for(Roster::class, new Request([
            'filter' => $request->only([
                'search', 'team', 'player', 'position'
            ]),
        ]))->allowedFilters(
                AllowedFilter::scope('team', 'byTeam'),
                AllowedFilter::scope('player', 'byPlayer'),
                AllowedFilter::scope('position', 'byPosition')
        )->join('player_totals', 'player_id', '=', 'id');


        return $query->get();
    }

    public function scopeByTeam(Builder $query, $team)
    {
        $query->where('team_code', '=', $team);
    }

    public function scopeByPlayer(Builder $query, $player)
    {
        $query->where('name', 'like', '%'.$player.'%');
    }

    public function scopeByPosition(Builder $query, $position)
    {
        $query->where('pos', '=', $position);
    }

    public function stats()
    {
        return $this->hasOne(PlayerStats::class, 'player_id', 'id');
    }

    public function getTotalPointsAttribute()
    {
        return ($this->field_goals * 2) + $this['3pt'] + $this->free_throws;
    }

    public function getTotalReboundsAttribute()
    {
        return $this->offensive_rebounds + $this->defensive_rebounds;
    }

    public function getFgPctAttribute()
    {
        if (!$this['field_goals_attempted']) {
            return 0.00;
        }
        $pct =  100 * $this['field_goals'] / $this['field_goals_attempted'];
        return number_format($pct, 2);
    }

    public function getThreePctAttribute()
    {
        if (!$this['three_pt_attempted']) {
            return 0.00;
        }
        $pct =  100 * $this['three_pt'] / $this['three_pt_attempted'];
        return number_format($pct, 2);
    }

    public function getFtPctAttribute()
    {
        if (!$this['free_throws_attempted']) {
            return 0.00;
        }
        $pct =  100 * $this['free_throws'] / $this['free_throws_attempted'];
        return number_format($pct, 2);
    }

    public function getTwoPctAttribute()
    {
        if (!$this['two_pt_attempted']) {
            return 0.00;
        }
        $pct =  100 * $this['two_pt'] / $this['two_pt_attempted'];
        return number_format($pct, 2);
    }
}
